/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.main

import android.bluetooth.BluetoothAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import de.ccc.events.badge.card10.CARD10_BLUETOOTH_MAC_PREFIX
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.filetransfer.FileTransferFragment
import de.ccc.events.badge.card10.hatchery.AppListFragment
import de.ccc.events.badge.card10.mood.MoodFragment
import de.ccc.events.badge.card10.scanner.ScannerFragment
import de.ccc.events.badge.card10.sparkle.BeautifulFragment
import de.ccc.events.badge.card10.time.TimeUpdateDialog
import kotlinx.android.synthetic.main.main_fragment.*
import java.lang.IllegalStateException
import java.sql.Connection

class MainFragment : Fragment(), GattListener {
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.main_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        button_pair.setOnClickListener { startFragment(ScannerFragment()) }
        button_mood.setOnClickListener { startFragment(MoodFragment()) }
        button_beautiful.setOnClickListener { startFragment(BeautifulFragment()) }
        button_hatchery.setOnClickListener { startFragment(AppListFragment()) }
        button_send.setOnClickListener { startFragment(FileTransferFragment()) }

        button_set_time.setOnClickListener {
            val dialogFragment = TimeUpdateDialog()
            dialogFragment.show(fragmentManager, "time")
            dialogFragment.setTime()
            dialogFragment.dismiss()
        }

        ConnectionService.addGattListener("main", this)

        val bondedCard10s =
            bluetoothAdapter.bondedDevices.filter { it.address.startsWith(CARD10_BLUETOOTH_MAC_PREFIX, true) }

        if (bondedCard10s.isNotEmpty()) {
            val ctx = activity ?: throw IllegalStateException()
            ConnectionService.connect(ctx)
            showConnectingView()
        } else {
            label_status.text = getString(R.string.main_label_not_connected)
            showDisconnectedView()
        }
    }

    private fun startFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun showConnectedView() {
        activity?.runOnUiThread {
            container_connected.visibility = View.VISIBLE
            container_disconnected.visibility = View.GONE
            button_pair.text = getString(R.string.main_button_manage_pairings)

            button_hatchery.isEnabled = true
            button_send.isEnabled = true
            button_mood.isEnabled = true
            button_beautiful.isEnabled = true
            button_set_time.isEnabled = true

            val device = ConnectionService.device
            label_status.text = getString(R.string.main_label_connected, device?.name, device?.address)
        }
    }

    private fun showConnectingView() {
        val device = ConnectionService.device
        label_status.text = getString(R.string.main_label_connecting, device?.name, device?.address)
        button_pair.text = getString(R.string.main_button_manage_pairings)
    }

    private fun showDisconnectedView() {
        container_connected.visibility = View.GONE
        container_disconnected.visibility = View.VISIBLE

        button_pair.text = getString(R.string.main_button_pair)
    }

    override fun onConnectionReady() {
        showConnectedView()
    }
}
