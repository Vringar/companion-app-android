/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.filetransfer

import android.bluetooth.BluetoothGatt
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.main.MainFragment
import kotlinx.android.synthetic.main.batch_transfer_fragment.*
import java.lang.Exception
import java.lang.IllegalStateException

private const val TAG = "BatchTransferFragment"

class BatchTransferFragment : Fragment(), FileTransferListener, GattListener {
    private lateinit var queue: TransferQueue
    private var transfer: FileTransfer? = null
    private var isCancelled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val args = arguments ?: throw IllegalStateException()
        val jobs = args.get("jobs") as? Array<TransferJob> ?: throw IllegalStateException()
        queue = TransferQueue(jobs)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.batch_transfer_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progress.max = queue.size

        button_cancel.setOnClickListener {
            isCancelled = true
        }

        button_done.setOnClickListener {
            val fragment = MainFragment()
            fragmentManager!!.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
        }

        startTransfer()
    }

    private fun startTransfer() {
        activity?.runOnUiThread {
            label_status.text = getString(R.string.batch_transfer_label_transferring)
            progress.max = queue.size
        }

        transferNext()
    }

    private fun transferNext() {
        Thread.sleep(1000)
        val item = queue.dequeue()

        if (item == null || isCancelled) {
            activity?.runOnUiThread {
                progress.progress = 0
                label_status.text = if (isCancelled) {
                    getString(R.string.batch_transfer_label_cancelled)
                } else {
                    getString(R.string.batch_transfer_label_complete)
                }
                button_cancel.visibility = View.GONE
                button_done.visibility = View.VISIBLE
            }
        } else {
            transferItem(item)
        }
    }

    private fun transferItem(transferJob: TransferJob) {
        try {
            val ctx = activity ?: throw IllegalStateException()
            val reader = ChunkedReader(ctx, transferJob.sourceUri, ConnectionService.mtu)
            val service = ConnectionService.leService ?: throw IllegalStateException()
            transfer = FileTransfer(service, reader,this, transferJob.destPath)
            transfer?.start()
        } catch (e: Exception) {
            Log.e(TAG, "Failed to initialize transfer")
            return
        }
    }

    override fun onError() {
        activity?.runOnUiThread {
            label_status.text = getString(R.string.batch_transfer_label_error)
            button_cancel.visibility = View.GONE
            button_done.visibility = View.VISIBLE
        }
    }

    override fun onFinish() {
        activity?.runOnUiThread {
            // TODO: Add workaround for broken progress bars
            // https://stackoverflow.com/questions/4348032/android-progressbar-does-not-update-progress-view-drawable
            progress.incrementProgressBy(1)
        }

        transferNext()
    }
}